//
//  Project39App.swift
//  Project39
//
//  Created by Eddington, Nick on 4/28/23.
//

import SwiftUI

@main
struct Project39App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
